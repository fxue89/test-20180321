import * as webpack from 'webpack'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import * as CompressionPlugin from 'compression-webpack-plugin'
import * as autoprefixer from 'autoprefixer'

export function createProdConfig(sourcePath: string) {
    return {
        module: {
            rules: [
                // .ts, .tsx
                {
                    test: /\.tsx?$/,
                    use: [
                        {
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: 'tsconfig.webpack.json',
                            }
                        }
                    ]
                },
                { // sass / scss loader for webpack
                    test: /\.s?css$/,
                    use: ExtractTextPlugin.extract({
                        use: [
                            "css-loader",
                            {
                                loader: "sass-loader",
                                options: {
                                    includePaths: [sourcePath]
                                }
                            },
                            {
                                loader: "postcss-loader",
                                options: {
                                    plugins: function () {
                                        return [autoprefixer]
                                    }
                                }
                            }
                        ],
                        // use style-loader in development 
                        fallback: "style-loader"
                    })
                },
            ]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin({}),
            new CopyWebpackPlugin([{
                from: 'assets',
                to: '../dist/assets'
            },
            {
                from: '*.ico',
                to: '../dist'
            }]),
            new CompressionPlugin({
                asset: "[path].gz[query]",
                algorithm: "gzip",
                test: /\.(js|css|html)$/, // index.html, manifest and some other files won't be compressed until a fix merged: https://github.com/webpack-contrib/compression-webpack-plugin/pull/45
                threshold: 10240,
                minRatio: 0.8
            }),
            new ExtractTextPlugin({
                filename: "[name].[contenthash].css"
            })
        ]
    }
}