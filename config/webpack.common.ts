import * as webpack from 'webpack'
import * as HtmlWebpackPlugin from 'html-webpack-plugin'
import * as ScriptExtHtmlWebpackPlugin from 'script-ext-html-webpack-plugin'
import { TsConfigPathsPlugin } from 'awesome-typescript-loader'
import { AppSettings } from "appSettings";

export const commonConfig = (appSettings: AppSettings, sourcePath: string, outPath: string) => {
    return {
        context: sourcePath,
        entry: {
            main: './index.tsx',
            vendor: [
                'react',
                'react-dom',
                'react-redux',
                'react-router',
                'redux'
            ]
        },
        output: {
            path: outPath,
            publicPath: appSettings.basePath,
            filename: '[name].bundle.js'
        },
        target: 'web',
        resolve: {
            extensions: ['.js', '.ts', '.tsx'],
            // Fix webpack's default behavior to not load packages with jsnext:main module
            // https://github.com/Microsoft/TypeScript/issues/11677 
            mainFields: ['browser', 'main'],
            plugins: [
                new TsConfigPathsPlugin()
            ]
        },
        module: {
            rules: [
                { test: /\.json$/, loader: 'json-loader' },
                // static assets 
                {
                    test: /\.html$/,
                    loader: 'html-loader'
                },
                {
                    test: /\.(png|woff|woff2|eot|ttf|svg|ico|jpg)$/,
                    loader: 'url-loader', // file sizes below the limit are loaded as data URLs,
                    options: { limit: 5120 }
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                    'appSettings': JSON.stringify(appSettings)
                }
            }),
            new webpack.LoaderOptionsPlugin({
                options: {
                    context: sourcePath
                }
            }),
            new webpack.optimize.AggressiveMergingPlugin(),
            new HtmlWebpackPlugin({
                template: 'index.html'
            }),
            new ScriptExtHtmlWebpackPlugin({
                defaultAttribute: 'defer'
            })
        ],
        node: {
            // workaround for webpack-dev-server issue 
            // https://github.com/webpack/webpack-dev-server/issues/60#issuecomment-103411179
            fs: 'empty',
            net: 'empty'
        }
    }
}
