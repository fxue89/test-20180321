import * as React from 'react'
import { Provider } from "react-redux"
import { ConnectedRouter } from "react-router-redux"
import { Switch, Route, RouteProps } from "react-router"
import { Store } from "redux"
import { AppState } from "store/app/AppState"
import { RouteMap } from 'components/steps/RouteMap'
import { History } from 'history'
import { StepHeader } from 'components/steps/StepsHeader'
import { Footer } from 'components/Footer'
import { Details1StepContainer } from 'components/steps/Details1Step';
import { Details2Step, Details2StepContainer } from 'components/steps/Details2Step';
import { googleAddressAutocomplete } from 'components/address/googleAddressAutocomplete';
import { PersistGate } from 'redux-persist/integration/react';
import { Persistor } from 'redux-persist';
import { Start } from 'components/Start';
import { Success } from 'components/Success';

interface RootProps {
    store: Store<AppState>
    history: History
    persistor: Persistor
}

export function Root(props: RootProps) {
    return (
        <Provider store={props.store}>
            <PersistGate loading={null} persistor={props.persistor}>
                <ConnectedRouter history={props.history} >
                    <main>
                        <Switch>
                            <Route path={RouteMap.StepsRoot} render={(x: RouteProps) => <StepHeader currentPath={x.location ? x.location.pathname : undefined} />} />
                        </Switch>
                        <div className="steps-main-wrapper">
                            <Switch>
                                <Route exact path={RouteMap.Root} render={Start} />
                                <Route path={RouteMap.Details1} render={() => <Details1StepContainer />} />
                                <Route path={RouteMap.Details2} render={() => <Details2StepContainer addressAutocompleteFunc={googleAddressAutocomplete} />} />
                                <Route exact path={RouteMap.Complete} render={Success} />
                            </Switch>
                        </div>
                        <Footer />
                    </main>
                </ConnectedRouter>
            </PersistGate>
        </Provider>
    )
}