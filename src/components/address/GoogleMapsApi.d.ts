/**
 * Definitions for the Google Maps API loader package. This thin wrapper is used to properly import libraries instead of loading in the `index.html` file.
 */
declare module 'google-maps-api' {

    declare interface Prediction {
        readonly description: string

        readonly matched_substrings: ReadonlyArray<{
            readonly length: number
            readonly offset: number
        }>

        readonly terms: ReadonlyArray<{
            readonly offset: number
            readonly value: string
        }>

        readonly types: ReadonlyArray<"street_address" | "geocode">
    }

    declare function GoogleMapsApiLoader(googleApiKey: string, libraries: ReadonlyArray<string>): () => Promise<typeof google.maps>

    export = GoogleMapsApiLoader
}
