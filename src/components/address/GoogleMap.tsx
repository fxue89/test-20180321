import './GoogleMap.scss'

import * as React from 'react'
import { appSettings } from 'appSettings';
import { maps$ } from 'components/address/googleAddressAutocomplete';
import { Address } from 'store/app/steps/Details2';
import { AppState } from 'store/app/AppState';
import { connect, Dispatch } from 'react-redux';
import { actions } from 'react-redux-form';

interface Props {
    location: Address
    onMapClick(address: Address): Dispatch<Address>
}

interface State {
    lastLocation: {
        lat: Number
        lng: Number
    }
    zoom: Number
}
const melbourne = { lat: -37.8136, lng: 144.9631 };

export default class GoogleMap extends React.Component<Props, State> {
    state = {
        lastLocation: melbourne,
        zoom: 12
    }
    private mapClickListener = (map, geocoder) => {
        map.addListener('click', event => {
            geocoder.geocode({
                location: event.latLng
            }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        this.props.onMapClick({ lat: event.latLng.lat(), lng: event.latLng.lng(), input: results[0].formatted_address })
                    }
                }
            });
        });
    }

    private mapZoomListener = map => {
        map.addListener('zoom_changed', () => {
            this.setState({
                zoom: map.getZoom(),
            });
        });
    }

    componentDidMount() {
        maps$.then(maps => {
            const geocoder = new maps.Geocoder();
            if (this.props.location.lat && this.props.location.lng) {
                this.setState({
                    lastLocation: {
                        lat: this.props.location.lat,
                        lng: this.props.location.lng
                    }
                });
            }
            let map = new maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: this.state.lastLocation
            });
            if (this.props.location.lat && this.props.location.lng) {
                let marker = new maps.Marker({
                    position: this.state.lastLocation,
                    map: map
                });
            }
            this.mapClickListener(map, geocoder)
            this.mapZoomListener(map)
        });

    }

    componentWillReceiveProps(nextprops: Props) {
        if (nextprops.location.lat && nextprops.location.lng) {
            this.setState({
                lastLocation: {
                    lat: nextprops.location.lat,
                    lng: nextprops.location.lng
                }
            })
            maps$.then(maps => {
                const geocoder = new maps.Geocoder();
                let map = new maps.Map(document.getElementById('map'), {
                    zoom: this.state.zoom,
                    center: this.state.lastLocation
                });
                let marker = new maps.Marker({
                    position: { lat: nextprops.location.lat, lng: nextprops.location.lng },
                    map: map
                });
                this.mapClickListener(map, geocoder)
                this.mapZoomListener(map)
            });
        }
    }

    render() {
        return (
            <div id='map' />
        );
    }
};

function mapStateToProps(state: AppState) {
    return {
        location: state.steps.details2.address.autocompleted
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onMapClick: (address: Address) => {
            dispatch(actions.change('steps.details2.address.autocompleted', address))
        }
    }
}

export const GoogleMapContainer = connect(mapStateToProps, mapDispatchToProps)(GoogleMap)