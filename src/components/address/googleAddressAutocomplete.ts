/**
 * Google maps autocompletion function.
 */

import { appSettings } from 'appSettings'
import { AddressResolver, AddressListItem } from './AddressAutocomplete'
import GoogleMapsApiLoader = require('google-maps-api')
import { AustralianStateAbbreviation } from 'store/app/AppState';
import { Address } from 'store/app/steps/Details2';

export const maps$: Promise<typeof google.maps> = GoogleMapsApiLoader(appSettings.googleApiKey, ['places'])();

type AddressComponentType = "subpremise" | "street_number" | "street_address" | "route" | "locality" | "administrative_area_level_1" | "postal_code" | "country"

/**
 * Extension over google maps typing package.
 */
interface ExtendedAutocompletePrediction {
    readonly structured_formatting: {
        readonly main_text: string
        readonly secondary_text: string
    }
}

type AddressComponent = google.maps.GeocoderAddressComponent & { readonly types: ReadonlyArray<AddressComponentType> }

function getAddressComponent(components: ReadonlyArray<AddressComponent>, type: AddressComponentType): string | undefined {
    const component = components.find(x => x.types.some(t => t === type));
    return component ? component.long_name : undefined;
}

function getAustralianStateByName(name: string): AustralianStateAbbreviation | undefined {
    switch (name) {
        case 'Australian Capital Territory': return AustralianStateAbbreviation.ACT;
        case 'New South Wales': return AustralianStateAbbreviation.NSW;
        case 'Northern Territory': return AustralianStateAbbreviation.NT;
        case 'Queensland': return AustralianStateAbbreviation.QLD;
        case 'South Australia': return AustralianStateAbbreviation.SA;
        case 'Tasmania': return AustralianStateAbbreviation.TAS;
        case 'Victoria': return AustralianStateAbbreviation.VIC;
        case 'Western Australia': return AustralianStateAbbreviation.WA;
        default: return undefined;
    }
}

function createAddressFromPlaceResult(place: google.maps.places.PlaceResult): Partial<Address> {
    const components = place.address_components as any as ReadonlyArray<AddressComponent>;
    const stateName = getAddressComponent(components, "administrative_area_level_1");
    const addressLine1Fragments = [
        getAddressComponent(components, "street_number"),
        getAddressComponent(components, "street_address") || getAddressComponent(components, "route")
    ];

    const geometryInfo = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
    }
    return {
        lat: geometryInfo.lat,
        lng: geometryInfo.lng
    };
}

function getAddressResolver(prediction: google.maps.places.AutocompletePrediction): AddressResolver {
    let address: any = null;
    return () => new Promise(resolve => {
        maps$.then(x => {
            const placesService = new x.places.PlacesService(document.createElement('div'));
            if (!address) {
                return placesService.getDetails(
                    { placeId: prediction.place_id },
                    place => {
                        address = createAddressFromPlaceResult(place);
                        resolve(address);
                    });
            }
        });
    });
}

function createAddressItemFromPrediction(prediction: google.maps.places.AutocompletePrediction & ExtendedAutocompletePrediction): AddressListItem {
    return {
        description: prediction.structured_formatting.main_text + ' ' + prediction.structured_formatting.secondary_text.replace(', Australia', ''), // TODO: automatically remove country name?
        getAddress: getAddressResolver(prediction) as any
    }
}

function getPlacePredictions(input: string, service: google.maps.places.AutocompleteService, location?: google.maps.LatLng): Promise<ReadonlyArray<google.maps.places.AutocompletePrediction>> {
    return new Promise(resolve => {
        const requestData: google.maps.places.AutocompletionRequest = { input, types: ['address'], location, componentRestrictions: { country: 'au' } };
        service.getPlacePredictions(
            requestData, // TODO: refactor into external options
            x => resolve(x || []));
    });
}

export async function googleAddressAutocomplete(input: string, location?: Coordinates): Promise<ReadonlyArray<AddressListItem>> {
    if (input.trim() === '') {
        return [];
    }

    const maps = await maps$;
    const service = new maps.places.AutocompleteService();
    const latLng = location ? new maps.LatLng(location.latitude, location.longitude) : undefined;
    const predictions = await getPlacePredictions(input, service, latLng);
    return predictions.map(createAddressItemFromPrediction);
}