import * as loadingImageUrl from 'assets/img/loading-circle.png'
import * as poweredByGoogleLogoUrl from 'assets/img/powered-by-google.png'

import './AddressAutocomplete.scss'

import * as React from 'react'
import * as classNames from 'classnames'
import { Address } from "store/app/steps/Details2"
import { Key } from 'ts-keycode-enum'

function debounce<T extends Function>(func: T, getWait: () => number): T {
    let timeout: any = null;

    return function (this: any, ...args) {
        clearTimeout(timeout);
        timeout = setTimeout(() => func.apply(this, args), getWait());
    } as any
}

export type AddressResolver = () => Promise<Partial<Address>>

export type AddressAutocompleteFunc = (input: string, location?: Coordinates) => Promise<ReadonlyArray<AddressListItem>>

export interface AddressListItem {
    readonly description: string
    readonly getAddress: AddressResolver
}

interface AddressAutocompleteProps {
    readonly debounce: number
    readonly placeholder: string
    readonly autocomplete: AddressAutocompleteFunc
    readonly wrapperClassName?: string
    readonly noResultsComponent: React.ComponentType<{ close: () => void }>
    readonly value: Partial<Address> | undefined
    readonly onChange: (value: Partial<Address>) => void
    readonly onFocus: (event: React.FocusEvent<HTMLInputElement>) => void
    readonly onBlur: (event: React.FocusEvent<HTMLInputElement>) => void
}

interface AddressAutocompleteState {
    readonly selectedSuggestion: AddressListItem | undefined
    readonly isOpen: boolean
    readonly isLoading: boolean
    readonly suggestions: ReadonlyArray<AddressListItem>
}

interface DropDownListProps extends React.HTMLProps<HTMLUListElement> {
    readonly items: ReadonlyArray<AddressListItem>
    readonly onSelectedItemChange: (item: AddressListItem) => void
    readonly selectedItem: AddressListItem | undefined
}

function DropDownList({ items, onSelectedItemChange, selectedItem, ...otherProps }: DropDownListProps) {
    return (
        <ul {...otherProps}>
            {
                items.map((item, index) =>
                    <li role="option" key={index} className={classNames({ checked: item === selectedItem })}
                        onClick={() => onSelectedItemChange(item)}>{item.description}</li>
                )
            }
        </ul>
    )
}

/**
 * Address autocomplete.
 */
export class AddressAutocomplete extends React.Component<AddressAutocompleteProps, AddressAutocompleteState> {

    private inputElement: HTMLInputElement | null = null

    private wrapperElement: HTMLElement | null = null

    readonly state: AddressAutocompleteState;

    constructor(props: AddressAutocompleteProps) {
        super(props);
        this.state = { isOpen: false, selectedSuggestion: undefined, suggestions: [], isLoading: false };
    }

    private readonly blurIfEventIsOutside = (event: Event) => {
        if (!this.props.wrapperClassName!.includes("untouched") && event.target instanceof HTMLElement && this.wrapperElement && !this.wrapperElement.contains(event.target)) {
            this.close();
            this.props.onBlur(null as any);
        }
    }

    componentWillMount() {
        document.addEventListener('mousedown', this.blurIfEventIsOutside);
        document.addEventListener('focusin', this.blurIfEventIsOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.blurIfEventIsOutside);
        document.removeEventListener('focusin', this.blurIfEventIsOutside);
    }

    private readonly select = (selectedSuggestion: AddressListItem, keepOpen = false) => {
        if (this.inputElement) {
            this.inputElement.focus();
        }

        this.setState({ isOpen: keepOpen, selectedSuggestion });

        this.props.onChange({ input: selectedSuggestion.description })
        selectedSuggestion
            .getAddress()
            .then(address => {
                this.props.onChange({ ...address, input: selectedSuggestion.description });
            } );
    }

    private readonly handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const input = event.target.value;
        const isBlank = input.trim() === '';
        if (isBlank) {
            this.setState({ selectedSuggestion: undefined, suggestions: [], isOpen: false });
            this.props.onChange({ input });
        }
        else {
            this.setState({ selectedSuggestion: undefined });
            this.props.onChange({ input });
            this.debounceAutocomplete(input);
        }
    }

    private readonly autocomplete = (input: string): void => {
        this.setState({ isLoading: true });
        this.props
            .autocomplete(input)
            .then(suggestions => {
                this.setState({ isLoading: false, isOpen: true, suggestions });
            })
            .catch(error => console.error(error)); // MAYBE: send us logs about the error
    }

    private readonly debounceAutocomplete = debounce(this.autocomplete, () => this.props.debounce)

    private readonly handleInputRef = (ref: HTMLInputElement | null) => this.inputElement = ref

    private readonly handleWrapperRef = (ref: HTMLElement | null) => this.wrapperElement = ref

    private readonly close = () => this.setState({ isOpen: false })

    private getSelectedIndex(): number {
        return this.state.suggestions.indexOf(this.state.selectedSuggestion!);
    }

    private getNextIndex(key: Key.UpArrow | Key.DownArrow): number {
        const index = this.getSelectedIndex();
        if (index === -1) {
            return 0;
        }
        return key === Key.UpArrow
            ? (index + 1) === this.state.suggestions.length ? 0 : (index + 1)
            : (index - 1) < 0 ? (this.state.suggestions.length - 1) : (index - 1);
    }

    private readonly handleKeyDown = (event: React.KeyboardEvent<HTMLElement>) => {
        switch (event.keyCode) {
            case Key.Escape:
                if (this.state.isOpen) {
                    event.preventDefault();
                    this.close();
                }
                return;
            case Key.UpArrow:
            case Key.DownArrow: {
                event.preventDefault();
                if (!this.state.isOpen || this.state.suggestions.length === 0) {
                    return;
                }

                const nextIndex = this.getNextIndex(event.keyCode);
                this.select(this.state.suggestions[nextIndex], true);
                return;
            }
        }
    }

    render() {
        const noResults = this.state.suggestions.length === 0;
        const input = this.props.value ? this.props.value.input : '';
        return (
            <div role="listbox" ref={this.handleWrapperRef} className={classNames("control autocomplete drop-down", this.props.wrapperClassName, { open: this.state.isOpen, empty: !input })}>
                <label className="control-label control-label-animated">{this.props.placeholder}</label>
                <div className="drop-down-header">
                    <input type="text" onKeyDown={this.handleKeyDown} onBlur={this.props.onBlur} ref={this.handleInputRef} onChange={this.handleChange} onFocus={this.props.onFocus}
                        value={input} placeholder={this.props.placeholder} style={{ cursor: 'auto' }} />
                    {this.state.isLoading && <img className="loading" src={loadingImageUrl} />}
                </div>
                <span className="control-underline"></span>
                <div>
                    {
                        noResults
                            ? <div className="drop-down-body"><this.props.noResultsComponent close={this.close} /></div>
                            :
                            <div className="drop-down-body">
                                <DropDownList className="drop-down-list" onSelectedItemChange={this.select} items={this.state.suggestions} selectedItem={this.state.selectedSuggestion} />
                                <img src={poweredByGoogleLogoUrl} />
                            </div>
                    }
                </div>
            </div>
        )
    }
}