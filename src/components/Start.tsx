import * as React from 'react'
import { LinkButton } from 'components/shared/LinkButton';
import { RouteMap } from 'components/steps/RouteMap';

export function Start() {
    return (
        <div className="full-screen-description">
            <div>
                A simple web app takes you through a smooth application process
            </div>
            <LinkButton to={RouteMap.Details1}>
                Start
            </LinkButton>
        </div>
    )
}