import * as React from 'react'
import * as classNames from "classnames"

interface dialogBoxProps {
    readonly isVisible: boolean
    readonly wrapperClassName?: string
    readonly dismissible?: boolean
    readonly hide: () => void
    readonly children?: React.ReactNode
}

export function DialogBox(props: dialogBoxProps) {
    const dismissDialog = (event: React.MouseEvent<HTMLElement>) => {
        if (event.target === event.currentTarget && props.dismissible !== false) {
            props.hide();
        }
    }
    return (
        <div className={classNames("dialog-wrapper", { hidden: !props.isVisible }, props.wrapperClassName)} onClick={dismissDialog}>
            <article className="dialog">
                <button type="button" className="btn-close" onClick={props.hide}><span className="icon-times"></span></button>
                {props.children}
            </article>
        </div>
    )
}
