
/**
 * Decimal adjustment of a number.
 *
 * @param {String}  type  The type of adjustment.
 * @param {Number}  tempValue The number.
 * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
 * @returns {Number} The adjusted value.
 */
function decimalAdjust(type: string, value: number, exp: number): number {
    // If the exp is undefined or zero...
    if (exp === 0) {
        return Math[type](value);
    }
    let tempValue: any = value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (tempValue === null || isNaN(tempValue) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }
    // If the value is negative...
    if (tempValue < 0) {
        return -decimalAdjust(type, -tempValue, exp);
    }
    // Shift
    tempValue = tempValue.toString().split('e');
    tempValue = Math[type](+(tempValue[0] + 'e' + (tempValue[1] ? (+tempValue[1] - exp) : -exp)));
    // Shift back
    tempValue = tempValue.toString().split('e');
    return +(tempValue[0] + 'e' + (tempValue[1] ? (+tempValue[1] + exp) : exp));
}

export const DecimalMath = {
    round(value, exp) {
        return decimalAdjust('round', value, exp);
    }
}
