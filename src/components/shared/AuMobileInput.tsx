import * as React from 'react'
import MaskedInput, { MaskedInputProps } from 'react-text-mask'
import * as classNames from 'classnames'

const auMobileMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

export interface AuMobileInputProps extends MaskedInputProps, React.InputHTMLAttributes<HTMLInputElement> {
    wrapperClassName: string
}

export const AuMobileInput = ({ wrapperClassName, ...inputProps }: AuMobileInputProps) => (
    <div className={classNames("control", wrapperClassName, { empty: inputProps.value == undefined || inputProps.value === '' })}>
        <label className="control-label control-label-animated">{(inputProps as any).placeholder}</label>
        <MaskedInput mask={auMobileMask} guide={false} {...inputProps} />
        <span className="control-underline"></span>
    </div>
)