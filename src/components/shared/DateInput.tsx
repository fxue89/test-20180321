import 'components/shared/controls/Control.scss'
import './DateInput.scss'

import * as React from "react"
import { Parser } from "components/shared/parsers/Parser"
import MaskedInput from 'react-text-mask'
import * as classNames from "classnames"
import { DateOnly } from "core/DateOnly"
import { Key } from 'ts-keycode-enum'

interface ParsingWrapperProps<T> {
    readonly parsedValue: T
    readonly onParsedChange: (event: T) => void
    readonly onInputRef: (component: HTMLInputElement | null) => void
    readonly parser: Parser<T>
    readonly mask: ReadonlyArray<RegExp | string>
    readonly type?: string
    readonly placeholder: string

    readonly onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void
    readonly onFocus?: (event: React.FocusEvent<HTMLInputElement>) => void

    readonly onNext?: () => void
    readonly onPrev?: () => void

    readonly value?: undefined // disallow
    readonly onChange?: undefined // disallow
}

interface ParsingWrapperState {
    displayedValue: string
}

class ParsingWrapper<T> extends React.Component<ParsingWrapperProps<T>, ParsingWrapperState> {

    private lastParsedValue: T;

    constructor(props: ParsingWrapperProps<T>) {
        super(props);
        this.lastParsedValue = props.parsedValue;
        this.state = { displayedValue: props.parser.format(props.parsedValue) };
    }

    componentWillReceiveProps(nextProps: ParsingWrapperProps<T>): void {
        // update display only on external changes
        if (nextProps.parsedValue != this.lastParsedValue) {
            this.setState({ displayedValue: nextProps.parser.format(nextProps.parsedValue) });
        }
    }

    private handleBlur = (event: React.FocusEvent<HTMLInputElement>): void => {
        this.setState({ displayedValue: this.props.parser.format(this.props.parsedValue) });
        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    }

    private handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.lastParsedValue = this.props.parser.parse(event.target.value);
        if (this.state.displayedValue != event.target.value) {
            this.setState({ displayedValue: event.target.value });
        }

        this.props.onParsedChange(this.lastParsedValue);
    }

    private handleInputRef = (component: MaskedInput) => {
        if (this.props.onInputRef != null && component != null) {
            this.props.onInputRef(component.inputElement);
        }
    }

    private isBackKeyCode(keyCode: number) {
        return keyCode === Key.Backspace || keyCode === Key.LeftArrow;
    }

    private handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (this.props.onPrev && event.currentTarget.selectionStart == 0 && event.currentTarget.selectionEnd == 0) {
            if (this.isBackKeyCode(event.keyCode)) {
                this.props.onPrev();
                event.preventDefault();
            }
        }
    }

    private handleKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (this.props.onNext && event.currentTarget.selectionStart == this.props.mask.length && event.currentTarget.selectionEnd == this.props.mask.length) {
            if (!this.isBackKeyCode(event.keyCode)) {
                this.props.onNext();
            }
        }
    }

    render() {
        const { parsedValue, onParsedChange, onNext, onPrev, parser, onInputRef, mask, ...inputProps } = this.props;
        return (
            <div>
                <MaskedInput
                    {...inputProps as any} // TODO: fix typing
                    mask={mask}
                    guide={false}
                    ref={this.handleInputRef}
                    onChange={this.handleChange}
                    onBlur={this.handleBlur}
                    value={this.state.displayedValue}
                    onKeyDown={this.handleKeyDown}
                    onKeyUp={this.handleKeyUp}
                />
                <span className="control-underline"></span>
            </div>
        )
    }
}

class DatePartInput extends ParsingWrapper<number | undefined> { }

interface DateInputProps {
    // Allows control to detect its higher-level focus state and css class 
    placeholder: string
    wrapperClassName?: string
    onFocus?(event: React.FocusEvent<HTMLInputElement>): void
    onBlur?(event: React.FocusEvent<HTMLInputElement>): void

    onChange(value?: Partial<DateOnly>): void
    value?: Partial<DateOnly>
}

function formatDatePart(value: number | undefined) {
    return (value != undefined && value !== 0 && !isNaN(value))
        ? value.toString()
        : '';
}

function getFullYear(twoDigitYear: number): number {
    const fullYear = 2000 + twoDigitYear;
    const nowFullYear = new Date().getFullYear();
    const isFutureYear = fullYear >= nowFullYear;
    return isFutureYear ? fullYear - 100 : fullYear;
}

const dayOrMonthParser: Parser<number | undefined> = {
    format(value: number | undefined) {
        const formattedNumber = formatDatePart(value);
        return formattedNumber ? ('00' + formattedNumber).substr(-2) : '';
    },
    parse: parseInt
}

const yearParser: Parser<number | undefined> = {
    format: formatDatePart,
    parse: parseInt
}

export class DateInput extends React.Component<DateInputProps> {

    private dayRef: HTMLInputElement | null = null

    private monthRef: HTMLInputElement | null = null

    private yearRef: HTMLInputElement | null = null

    render() {
        return (
            <div className={classNames('date-input control', this.props.wrapperClassName)}>
                <label className="control-label control-label-animated">{this.props.placeholder}</label>
                <div className="date-input-body">
                    <DatePartInput type="tel" mask={[/\d/, /\d/]}
                        onInputRef={this.handleDayRefChange}
                        parser={dayOrMonthParser}
                        parsedValue={this.props.value ? this.props.value.day : undefined}
                        onParsedChange={day => this.changeWithDatePart({ day })}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        onNext={this.focusMonth}
                        placeholder="DD"
                    />
                    <div>/</div>
                    <DatePartInput type="tel" mask={[/\d/, /\d/]}
                        onInputRef={this.handleMonthRefChange}
                        parser={dayOrMonthParser}
                        parsedValue={this.props.value ? this.props.value.month : undefined}
                        onParsedChange={month => this.changeWithDatePart({ month })}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        onPrev={() => this.focusDay(false)}
                        onNext={this.focusYear}
                        placeholder="MM" />
                    <div>/</div>
                    <DatePartInput type="tel" mask={[/\d/, /\d/, /\d/, /\d/]}
                        onInputRef={this.handleYearRefChange}
                        parser={yearParser}
                        parsedValue={this.props.value ? this.props.value.year : undefined}
                        onParsedChange={year => this.changeWithDatePart({ year })}
                        onFocus={this.handleFocus}
                        onBlur={event => {
                            if (this.props.value != undefined && this.props.value.year != undefined && this.props.value.year < 100) {
                                this.changeWithDatePart({ year: getFullYear(this.props.value.year) });
                            }

                            // setTimeout to allow year change to propagate before onBlur validation gets triggered.
                            event.persist();
                            setTimeout(() => this.handleBlur(event), 1);
                        }}
                        onPrev={() => this.focusMonth(false)}
                        placeholder="YYYY" />
                </div>
                {this.props.children}
            </div>
        )
    }

    private readonly changeWithDatePart = (part: Partial<DateOnly>) => {
        this.props.onChange({ ...this.props.value, ...part });
    }

    /**
     * Allows hopping to the next input after value of the current one have been filled
     * @param input Next input to focus
     */
    private focusInput(input: HTMLInputElement, start: boolean) {
        const cursorPosition = start ? 0 : input.value.length;
        input.selectionStart = cursorPosition;
        input.selectionEnd = cursorPosition;
        input.focus();
    }

    private readonly focusDay = (start: boolean = true) => {
        if (this.dayRef) {
            this.focusInput(this.dayRef, start);
        }
    }

    private readonly focusMonth = (start: boolean = true) => {
        if (this.monthRef) {
            this.focusInput(this.monthRef, start);
        }
    }

    private readonly focusYear = (start: boolean = true) => {
        if (this.yearRef) {
            this.focusInput(this.yearRef, start);
        }
    }

    private handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
        if (!this.props.onFocus) {
            return;
        }

        if (!this.isFocused) {
            this.isFocused = true;
            this.props.onFocus(event);
        }
    }

    private handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
        if (!this.props.onBlur) {
            return;
        }

        if (event.relatedTarget != this.dayRef && event.relatedTarget != this.monthRef && event.relatedTarget != this.yearRef) {
            this.isFocused = false;
            this.props.onBlur(event);
        }
    }

    private readonly handleDayRefChange = (ref: HTMLInputElement | null) => this.dayRef = ref

    private readonly handleMonthRefChange = (ref: HTMLInputElement | null) => this.monthRef = ref

    private readonly handleYearRefChange = (ref: HTMLInputElement | null) => this.yearRef = ref

    private isFocused = false
}