import './AuPhoneInput.scss'
import * as React from 'react'
import MaskedInput from 'react-text-mask'
import * as classNames from "classnames"
import { Key } from 'ts-keycode-enum'

export const auPhoneMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
interface Props {
    readonly onChange: (value: string) => void,
    readonly onFocus: (event: React.FocusEvent<HTMLInputElement>) => void,
    readonly onBlur: (event: React.FocusEvent<HTMLInputElement>) => void,
    readonly value: string
    readonly wrapperClassName: string
    readonly placeholder: string
}
export class AuPhoneInput extends React.Component<Props, any> {
    private splitPhoneNumber(value: string) {
        return {
            areaCode: value.substring(0, 2),
            localNumber: value.substring(2)
        }
    }

    private readonly handleAreaCodeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const parts = this.splitPhoneNumber(this.props.value);
        let emptyAreaCode = "";
        if (event.currentTarget.value.length === 1) {
            emptyAreaCode = " ";
        } else if (event.currentTarget.value.length === 0) {
            emptyAreaCode = "  ";
        }
        if (parts.localNumber === "") {
            emptyAreaCode = "";
        }
        const newValue = event.currentTarget.value + emptyAreaCode + parts.localNumber;
        this.props.onChange(newValue);
    }

    private readonly handleLocalNumberChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const parts = this.splitPhoneNumber(this.props.value);
        const newValue = parts.areaCode + event.currentTarget.value;
        this.props.onChange(newValue);
    }

    private areaCodeRef: HTMLInputElement | null
    private localNumberRef: HTMLInputElement | null

    render() {
        const parts = this.splitPhoneNumber(this.props.value);

        return (
            <div className={classNames("control", this.props.wrapperClassName, { empty: this.props.value == undefined || this.props.value === ''})}>
                <label className="control-label control-label-animated">{this.props.placeholder}</label>
                <div className="area-phone-input">
                    <span />
                    <MaskedInput
                        ref={this.handleAreaCodeRefChange}
                        mask={[/\d/, /\d/]}
                        guide={false}
                        className="area-code"
                        type="tel"
                        onChange={this.handleAreaCodeChange}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        value={parts.areaCode}
                        onKeyUp={this.focusLocalNumberIfAreaCodeCompleted} />
                    <MaskedInput
                        type="tel"
                        ref={this.handleLocalNumberRefChange}
                        mask={auPhoneMask}
                        guide={false}
                        onChange={this.handleLocalNumberChange}
                        onFocus={this.handleLocalNumberFocus}
                        onBlur={this.handleBlur}
                        value={parts.localNumber}
                        onKeyUp={this.focusAreaCodeIfLocalNumberOnBack} />
                </div>
                <div className="control-underline"></div>
            </div>
        )

    }

    private focusInputStart(input: HTMLInputElement, cursorPosition: number) {
        input.selectionStart = cursorPosition;
        input.selectionEnd = cursorPosition;
        input.focus();
    }

    private readonly focusLocalNumberIfAreaCodeCompleted = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (this.localNumberRef && event.currentTarget.selectionStart === 2) {
            this.focusInputStart(this.localNumberRef, 0);
        }
    }
    private readonly focusAreaCodeIfLocalNumberOnBack = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (this.areaCodeRef && (event.keyCode === Key.Backspace || event.keyCode === Key.LeftArrow) && event.currentTarget.selectionStart === 0) {
            this.focusInputStart(this.areaCodeRef, 2);
        }
    }

    private readonly handleLocalNumberRefChange = (ref: MaskedInput | null) => this.localNumberRef = ref ? ref.inputElement : null
    private readonly handleAreaCodeRefChange = (ref: MaskedInput | null) => this.areaCodeRef = ref ? ref.inputElement : null

    private isFocused = false;
    private handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
        if (!this.props.onBlur) {
            return;
        }

        if (event.relatedTarget != this.localNumberRef) {
            this.isFocused = false;
            this.props.onBlur(event);
        }
    }
    private handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
        if (!this.props.onFocus) {
            return;
        }

        if (!this.isFocused) {
            this.isFocused = true;
            this.props.onFocus(event);
        }
    }
    private handleLocalNumberFocus = (event: React.FocusEvent<HTMLInputElement>) => {
        if (event.currentTarget.selectionStart === 0 && this.props.value.length < 2 && this.areaCodeRef) {
            this.focusInputStart(this.areaCodeRef, 2);
        }
        if (!this.props.onFocus) {
            return;
        }

        if (!this.isFocused) {
            this.isFocused = true;
            this.props.onFocus(event);
        }
    }
}