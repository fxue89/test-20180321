import './Control.scss'
import './DropDownControl.scss'
import * as React from 'react'
import { ListItem } from "components/shared/ListItem"
import { ControlProps, Control } from 'react-redux-form'
import * as classNames from "classnames"
import { getControlStateClassNames } from "components/shared/controls/getControlStateClassNames"
import { Key } from 'ts-keycode-enum'

export function DropDownControl({ ...otherProps }: ControlProps<HTMLInputElement>
    & { items: ReadonlyArray<ListItem<any>> }) {
    return <Control.custom
        {...otherProps as any}
        component={DropDown}
        validateOn="change"
        mapProps={{
            wrapperClassName: getControlStateClassNames,
            onSelectedValueChange: x => x.onChange,
            selectedValue: x => x.modelValue,
            onWrapperFocus: x => x.onFocus,
            onWrapperBlur: x => x.onBlur,
            ...otherProps.mapProps as any
        }} />
}

export interface DropDownProps<T> extends React.HTMLProps<HTMLInputElement> {
    items: ReadonlyArray<ListItem<T>>
    onSelectedValueChange: (value: T) => void
    selectedValue: T
    wrapperClassName: string
    onWrapperFocus: () => void
    onWrapperBlur: () => void
}

export interface DropDownState {
    isOpen: boolean
}

export class DropDown<T> extends React.Component<DropDownProps<T>, DropDownState> {

    readonly state = { isOpen: false }

    private readonly select = (value: T) => {
        this.props.onSelectedValueChange(value as any);
        this.close();
    }

    private readonly toggle = () => {
        this.setState(prevState => ({ isOpen: !prevState.isOpen }));
    }

    private readonly close = () => {
        this.setState({ isOpen: false });
    }

    private readonly handleWrapperBlur = () => {
        this.close();
        this.props.onWrapperBlur();
    }

    private getSelectedIndex(): number {
        return this.props.items.findIndex(item => item.value === this.props.selectedValue);
    }

    private getNextIndex(key: Key.UpArrow | Key.DownArrow): number {
        const index = this.getSelectedIndex();
        if (index === -1) {
            return 0;
        }
        return key === Key.UpArrow
            ? (index + 1) === this.props.items.length ? 0 : (index + 1)
            : (index - 1) < 0 ? (this.props.items.length - 1) : (index - 1);
    }

    private readonly handleKeyDown = (event: React.KeyboardEvent<HTMLElement>) => {
        switch (event.keyCode) {
            case Key.Escape:
                if (this.state.isOpen) {
                    event.preventDefault();
                    this.close();
                }
                return;
            case Key.Enter:
            case Key.Space:
                event.preventDefault();
                const index = this.getSelectedIndex();
                if (index === -1 && this.props.items.length > 0) {
                    this.props.onSelectedValueChange(this.props.items[0].value);;
                }
                this.toggle();
                return;
            case Key.UpArrow:
            case Key.DownArrow: {
                event.preventDefault();
                if (this.props.items.length === 0) {
                    return;
                }
                const nextIndex = this.getNextIndex(event.keyCode);
                this.props.onSelectedValueChange(this.props.items[nextIndex].value);
                return;
            }
        }
    }

    /**
     * `tabIndex` is `0` to make the whole thing focusable and produce blor/focus events. 
     * `input[type=text]` blur event propagation is stopped to not interfere with container's blur.
     */
    render() {
        const { wrapperClassName, items, onWrapperFocus, onWrapperBlur, selectedValue, onSelectedValueChange, ...divProps } = this.props;
        const selectedItem = items.find(x => x.value === selectedValue);
        const displayedValue = selectedItem ? selectedItem.text : '';
        return (
            <div role="listbox" tabIndex={0} onBlur={this.handleWrapperBlur} onKeyDown={this.handleKeyDown} onFocus={onWrapperFocus} className={classNames("control drop-down", wrapperClassName, { empty: selectedItem == undefined, open: this.state.isOpen })}>
                <label className="control-label control-label-animated">{divProps.placeholder}</label>
                <div className="drop-down-header" onClick={this.toggle}>
                    <input type="text" readOnly={true} {...divProps} value={displayedValue} />
                    <span>▾</span>
                </div>
                <span className="control-underline"></span>
                <ul className="drop-down-body drop-down-list">
                    {items.map((item, i) => this.renderItem(item, i, item == selectedItem, () => this.select(item.value)))}
                </ul>
            </div>
        )
    }

    private renderItem = (item: ListItem<T>, index: number, checked: boolean, select: () => void) => {
        return (
            <li role="option" key={index} className={classNames({ checked })} onMouseDown={select}>
                {item.text}
            </li>
        )
    }
}