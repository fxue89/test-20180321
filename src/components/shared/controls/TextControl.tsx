import './Control.scss'

import { ControlProps, Control } from "react-redux-form"
import * as React from "react"
import * as classNames from "classnames"
import { getControlStateClassNames } from "components/shared/controls/getControlStateClassNames"

export class TextControl extends React.Component<ControlProps<HTMLInputElement>> {

    private isEmpty(value: string | string[] | number | undefined) {
        return value == undefined || value === '';
    }

    private renderInner = ({ wrapperClassName, ...props }: React.HTMLProps<HTMLInputElement> & { wrapperClassName: string }) => {
        return (
            <div className={classNames("control", wrapperClassName, { empty: this.isEmpty(props.value) })}>
                <label className="control-label control-label-animated">{props.placeholder}</label>
                <input type="text" ref={this.props.ref} {...props} />
                <span className="control-underline"></span>
            </div>
        )
    }

    render() {
        const { ref, ...otherProps } = this.props;
        return <Control.text
            validateOn="change"
            {...otherProps}
            component={this.renderInner}
            mapProps={{ wrapperClassName: getControlStateClassNames }} />
    }
}