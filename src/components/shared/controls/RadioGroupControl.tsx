import './RadioGroupControl.scss'
import * as React from 'react'
import * as classNames from "classnames"
import { getControlStateClassNames } from "components/shared/controls/getControlStateClassNames"
import { ControlProps, Control } from "react-redux-form"

export interface RadioListItem<T> {
    readonly text: string
    readonly iconClassName?: string
    readonly value: T
}

interface RadioGroupProps<T> {
    readonly items: ReadonlyArray<RadioListItem<T>>
    readonly wrapperClassName: string
    readonly value: T
    readonly onFocus: () => void
    readonly onBlur: () => void
    readonly onChange: (value: T) => void
}

export class RadioGroup<T> extends React.Component<RadioGroupProps<T>> {
    render() {
        return (
            <div className={classNames("control radio-group", this.props.wrapperClassName)}>
                {
                    this.props.items.map((item, index) => (
                        <button key={index}
                            type="button"
                            className={classNames({ checked: this.props.value === item.value })}
                            onFocus={this.props.onFocus}
                            onBlur={this.props.onBlur}
                            onClick={() => {
                                if (this.props.value !== item.value) {
                                    this.props.onChange(item.value)
                                }
                            }} >
                            {item.iconClassName && <i className={item.iconClassName}></i>}
                            {item.text}
                        </button>
                    ))
                }
            </div>
        )
    }
}

export function RadioGroupControl(props: ControlProps<HTMLInputElement> & { items: ReadonlyArray<RadioListItem<any>> }) {
    return <Control.custom
        {...props as any}
        component={RadioGroup}
        mapProps={{
            wrapperClassName: getControlStateClassNames,
            onFocus: x => x.onFocus,
            onBlur: x => x.onBlur,
            onChange: x => x.onChange,
            value: x => x.modelValue
        }} />
}