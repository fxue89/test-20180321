/**
 * Definitions for 'react-text-mask' package until official ones are awailable.
 */
declare module 'react-text-mask' {
    export interface MaskedInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
        readonly mask: ReadonlyArray<string | RegExp>
        readonly placeholderChar?: string
        readonly guide?: boolean
    }
    
    class MaskedInput extends React.Component<MaskedInputProps, any> {
        inputElement: HTMLInputElement | null
    }

    export default MaskedInput
}