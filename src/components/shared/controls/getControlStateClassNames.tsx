import * as classNames from "classnames"
import { FieldState } from "react-redux-form"

export function getControlStateClassNames(mapProps: { fieldValue: FieldState }): string {
    return classNames(
        mapProps.fieldValue.pristine ? 'pristine' : 'dirty',
        mapProps.fieldValue.touched ? 'touched' : 'untouched',
        mapProps.fieldValue.valid ? 'valid' : 'invalid',
        mapProps.fieldValue.focus ? 'focus' : 'blur'
    );
}