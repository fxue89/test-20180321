import './BackButton.scss'
import * as React from 'react'
import { Link } from "react-router-dom"

export const BackButton = props => <Link className="back-arrow"
    {...props}
    onClick={() => {
        window.scrollTo(0, 0);
        if (props.onClick) {
            props.onClick();
        }
    }}>
    Back
</Link>