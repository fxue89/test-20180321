export interface Parser<T> {
    parse(input: string): T
    format(value: T): string
}