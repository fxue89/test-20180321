import * as React from 'react'
import { LinkButton } from 'components/shared/LinkButton';
import { RouteMap } from 'components/steps/RouteMap';

export function Success() {
    return (
        <div className="full-screen-description">
            <div>
                Thanks for taking this application.
                <br/>
                Your details are saved in your browser's local storage. Feel free to revisit your inputs.
            </div>
            <LinkButton to={RouteMap.Details1}>
                Review
            </LinkButton>
        </div>
    )
}