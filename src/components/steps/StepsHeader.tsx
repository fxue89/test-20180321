import './StepsHeader.scss'
import * as React from 'react'
import { StepSettings, ProgressBar } from "components/steps/ProgressBar"
import { RouteMap } from 'components/steps/RouteMap'

interface StepHeaderProps {
    readonly currentPath: string | undefined
}

const stepRoutes: StepSettings[] = [
    { to: RouteMap.Details1 },
    { to: RouteMap.Details2 }
]

export class StepHeader extends React.Component<StepHeaderProps> {
    constructor() {
        super();
        this.state = {
            isMobileScrolled: false
        }
    }

    render() {
        const currentStepIndex = stepRoutes.findIndex(x => {
            return this.props.currentPath! === x.to
        });
        const stepNames = ['About you', 'Address']
        const stepHeading = stepNames[currentStepIndex];
        const stepCount = stepRoutes.length;
        return (
            <header className="step-header">
                <div className="container-inner center">
                    <div className="phone fr"><i className="icon-phone"></i></div>
                    <div className="step-heading mb3-ns">{stepHeading}</div>
                    <ProgressBar currentStepIndex={currentStepIndex} stepCount={stepCount} />
                </div>
            </header>
        )
    }
}