import * as React from 'react'
import { Control, Form, Validators, Errors, actions } from 'react-redux-form'

import { BackButton } from "components/shared/BackButton"
import { ForwardButton } from "components/shared/LinkButton"

import { required, dateRequired, dateValid, yearBetween, email } from 'components/shared/Validators'

import { DateInput } from "components/shared/DateInput"
import { TextControl } from "components/shared/controls/TextControl"
import { getControlStateClassNames } from "components/shared/controls/getControlStateClassNames"
import { Redirect } from 'react-router'
import { Dispatch, connect } from 'react-redux';
import { RouteMap } from 'components/steps/RouteMap';

interface TextControlProps {
    model: string
    validators?: Validators
}

// TODO: merge DateInput into DateControl
function DateControl(props: { placeholder: string } & TextControlProps) {
    return <Control.custom
        component={DateInput}
        validators={props.validators} validateOn="blur"
        model={props.model} placeholder={props.placeholder}
        mapProps={{
            wrapperClassName: getControlStateClassNames,
            onChange: x => x.onChange,
            onFocus: x => x.onFocus,
            onBlur: x => x.onBlur,
            value: x => x.modelValue
        }} />
}

interface Detail1FormProps {
    onFormSubmit: () => void
}

class Details1Form extends React.Component<Detail1FormProps> {
    render() {
        return (
            <Form model="steps.details1" className="pb4 cf" onSubmit={this.props.onFormSubmit}>
                <div className="fl w-50 pr4-ns field-spacer">
                    <TextControl model=".firstName" placeholder="First name" validators={{ required }} maxLength={37} />
                    <Errors className="validation-container"
                        model=".firstName"
                        messages={{
                            required: 'First name is a required field'
                        }} />
                </div>

                <div className="fl w-50 pr field-spacer">
                    <TextControl model=".lastName" placeholder="Last name" validators={{ required }} maxLength={37} />
                    <Errors className="validation-container"
                        model=".lastName"
                        messages={{
                            required: 'Last name is a required field'
                        }} />
                </div>

                <div className="fl w-50-ns w-100 pr4-ns field-spacer">
                    <DateControl model=".dateOfBirth" placeholder="Date of birth" validators={{
                        dateRequired,
                        dateValid,
                        yearBetween: yearBetween(1900, new Date().getFullYear())
                    }} />
                    <Errors className="validation-container"
                        model=".dateOfBirth"
                        messages={{
                            dateRequired: 'Please enter a date',
                            dateValid: 'Please enter a valid date',
                            yearBetween: 'Please enter a valid date',
                        }} />
                </div>
                <div className="fl w-50-ns w-100 pr field-spacer">
                    <TextControl
                        validators={{ required, email }}
                        type="email"
                        model=".email" placeholder="Email" />
                    <Errors className="validation-container"
                        model=".email"
                        messages={{
                            required: 'Please enter an email address',
                            email: 'That\' is not a valid email address'
                        }} />
                </div>

            </Form>
        )
    }
}

interface Details1StepProps {
    onSubmit: () => void
}
export class Details1Step extends React.Component<Details1StepProps> {
    readonly state = { isSubmitted: false };
    readonly onFormSubmit = () => {
        this.setState({isSubmitted: true})
    }

    render() {
        return (
            <article className="article-main">
                <div>
                    <h1>Personal details</h1>
                    <Details1Form onFormSubmit={this.onFormSubmit} />
                </div>
                <div className="section-nav center step-ctrls tc">
                    {this.state.isSubmitted && <Redirect to={RouteMap.Details2} />}
                    <ForwardButton onClick={this.props.onSubmit} >Continue</ForwardButton>
                </div>
            </article>
        )
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onSubmit: () => {
            dispatch(actions.submit('steps.details1'))
        }
    }
}

export const Details1StepContainer = connect(undefined, mapDispatchToProps)(Details1Step)