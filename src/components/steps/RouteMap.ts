export enum RouteMap {
    Root = "/",
    StepsRoot = "/steps",
    Details1 = "/steps/details-1",
    Details2 = "/steps/details-2",
    Complete = "/success"
}