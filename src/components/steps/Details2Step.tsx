import './Details2Step.scss'

import { getControlStateClassNames } from 'components/shared/controls/getControlStateClassNames'
import * as React from 'react'
import { connect, Dispatch } from "react-redux"
import { Control, Fieldset, Form, Errors, actions } from 'react-redux-form'

import { BackButton } from "components/shared/BackButton"
import { ForwardButton } from "components/shared/LinkButton"
import { AddressAutocomplete, AddressAutocompleteFunc } from 'components/address/AddressAutocomplete'
import { autocompleteInputRequired, autocompleteSelectionRequired } from 'components/shared/Validators'
import { Redirect } from 'react-router'
import { RouteMap } from "components/steps/RouteMap"
import GoogleMap, { GoogleMapContainer } from 'components/address/GoogleMap';

function AddressAutocompleteNoResults(props: { readonly close: () => void }) {
    return (
        <div className="autocomplete-no-results">
            We couldn't find this address, please check your address or select on map
        </div>
    )
}

function AddressSet(props: { readonly model: string, readonly autocomplete: AddressAutocompleteFunc }) {
    return (
        <Fieldset model={props.model}>
            <div className="field-spacer">
                <Control.custom
                    model=".autocompleted"
                    placeholder="Address"
                    validateOn="change"
                    validators={{ autocompleteInputRequired, autocompleteSelectionRequired }}
                    component={AddressAutocomplete}
                    mapProps={{ onBlur: x => x.onBlur, onFocus: x => x.onFocus, onChange: x => x.onChange, value: x => x.modelValue, wrapperClassName: getControlStateClassNames }}
                    controlProps={{
                        autocomplete: props.autocomplete,
                        debounce: 250,
                        noResultsComponent: AddressAutocompleteNoResults
                    }} />
                <Errors className="validation-container"
                    model=".autocompleted"
                    messages={{
                        autocompleteInputRequired: 'Address is a required field',
                        autocompleteSelectionRequired: 'Please choose an address from the list'
                    }} />
            </div>
        </Fieldset>
    )
}

interface Props {
    readonly addressAutocompleteFunc: AddressAutocompleteFunc // TODO: remove?
    onSubmit: () => void
}


export class Details2Step extends React.Component<Props, { readonly isSubmitted: boolean }> {

    readonly state = { isSubmitted: false, };
    readonly onFormSubmit = () => {
        this.setState({ isSubmitted: true })
    }

    render() {
        return (
            <article className="article-main">
                <header>
                    <h1>Your address</h1>
                </header>
                <section>
                    <Form model="steps.details2" onSubmit={this.onFormSubmit}>
                        <AddressSet model=".address" autocomplete={this.props.addressAutocompleteFunc} />
                    </Form>
                    <GoogleMapContainer />
                </section>
                <section className="section-nav">
                    <BackButton to={RouteMap.Details1} />
                    {this.state.isSubmitted && <Redirect to={RouteMap.Complete} />}
                    <ForwardButton onClick={this.props.onSubmit} >Continue</ForwardButton>
                </section>
            </article >
        )
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onSubmit: () => {
            dispatch(actions.submit('steps.details2'))
        }
    }
}

export const Details2StepContainer = connect(undefined, mapDispatchToProps)(Details2Step)