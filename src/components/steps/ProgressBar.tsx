import './ProgressBar.scss'
import * as React from 'react'

export interface StepSettings { to: string }

interface ProgressBarProps {
    readonly currentStepIndex: number
    readonly stepCount: number
}
export class ProgressBar extends React.Component<ProgressBarProps> {
    render() {
        const style = {
            width: (this.props.currentStepIndex >= 0 ? Math.round((this.props.currentStepIndex + 1) / this.props.stepCount * 100) : 0) + "%"
        }
        return (
            <div className="cf">
                {(this.props.currentStepIndex !== -1 && this.props.currentStepIndex < this.props.stepCount) &&
                    <div className="step-index mr4-ns">Step {this.props.currentStepIndex + 1} of  {this.props.stepCount}</div>}
                <div className="progress-bar flex flex-row items-center">
                    <span className="filled-bar" style={style} />
                    {this.props.currentStepIndex < this.props.stepCount-1 && <span className="bar-tail" />}
                </div>
            </div>
        )
    }
}
