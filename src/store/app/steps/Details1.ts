import { DateOnly } from "core/DateOnly"

export interface Details1 {
    readonly firstName: string
    readonly lastName: string

    readonly dateOfBirth: Partial<DateOnly>
    readonly email: string
    readonly confirmEmail: string
}

export const Details1 = {
    Initial: {
        firstName: '',
        lastName: '',
        dateOfBirth: {},
        email: '',
        confirmEmail: ''
    } as Details1
}