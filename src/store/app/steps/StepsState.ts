import { Details1 } from "store/app/steps/Details1"
import { Details2 } from "store/app/steps/Details2"

export interface StepsState {
    readonly details1: Details1
    readonly details2: Details2
}

export const StepsState = {
    Initial: {
        details1: Details1.Initial,
        details2: Details2.Initial
    }
}