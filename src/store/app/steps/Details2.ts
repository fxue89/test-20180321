export interface Address {
    readonly input: string    
    readonly lat: number
    readonly lng: number
}

export interface AddressSet {
    readonly autocompleted: Partial<Address>
}

export const AddressSet = { 
    Initial: {
        autocompleted: { input: '' }
    }
}

export interface Details2 {
    readonly address: AddressSet
}

export const Details2 = {
    Initial: {
        address: AddressSet.Initial
    } as Details2
}