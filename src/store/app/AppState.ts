import { AddressSet } from "store/app/steps/Details2"
import { StepsState } from "./steps/StepsState";

export interface AppState {
    readonly steps: StepsState
}

export enum AustralianStateAbbreviation {
    ACT = 'ACT',
    NSW = 'NSW',
    NT = 'NT',
    QLD = 'QLD',
    SA = 'SA',
    TAS = 'TAS',
    VIC = 'VIC',
    WA = 'WA'
}

export const AppState = {
    Initial: {
        steps: StepsState.Initial
    } as AppState,
    Sample: {
        steps: {
            details1: {
                firstName: 'John',
                lastName: 'Burrows',
                dateOfBirth: { year: 1980, month: 4, day: 12 },
                email: 'john.burrows@test.local',
                confirmEmail: 'john.burrows@test.local'
            },
            details2: {
                address: {
                    ...AddressSet.Initial
                }
            }
        }
    } as AppState
}
