import { applyMiddleware, combineReducers, createStore, Store } from 'redux'
import logger from 'redux-logger'
import { History } from 'history'
import { routerMiddleware, routerReducer } from 'react-router-redux'
import { persistStore, persistReducer, Persistor } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import { AppState } from "store/app/AppState"
import { combineForms } from "react-redux-form"
import { AnyAction } from 'redux';

interface WindowWithTools {
    devToolsExtension?(): (args?: any) => any;
}

export class StoreHelper {

    constructor(
        private readonly history: History, 
        private readonly useLogger = true) {}

    createAndConfigureStore(defaultState: AppState = AppState.Initial): { store: Store<AppState>, persistor: Persistor } {
        const windowWithTools = window as WindowWithTools;
        const historyMiddleware = routerMiddleware(this.history);
    
        const create = windowWithTools.devToolsExtension
            ? windowWithTools.devToolsExtension()(createStore)
            : createStore;
    
        const initialState = { ...defaultState };
        const stepsReducer = combineForms(initialState.steps, 'steps');
    
        const reducers = combineReducers({
            router: routerReducer,
            steps: stepsReducer
        });

        const persistedReducer = persistReducer<AppState, AnyAction>({
            key: 'root',
            storage,
            blacklist: ([
                'notification',
                //'authentication' // is gets stuck in `isPending` state if the app is closed before a request is finished
                // REVIEW: blacklisting separate members of states
            ]) 
        }, reducers as any)
    
        const enhanceStoreCreator = this.useLogger ? applyMiddleware(historyMiddleware, logger) : applyMiddleware(historyMiddleware);
        const createStoreWithMiddleware = enhanceStoreCreator<AppState>(create);
        // return createStoreWithMiddleware(reducers, initialState) as Store<AppState>;
        const store = createStoreWithMiddleware(persistedReducer, defaultState);
        const persistor = persistStore(store);
        return { store, persistor };

    }
}