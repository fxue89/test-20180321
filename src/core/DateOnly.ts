export interface DateOnly {
    readonly day: number
    readonly month: number
    readonly year: number
}

export class DateOnly {
    static toDate(value: DateOnly): Date {
        return new Date(value.year, value.month - 1, value.day);
    }
}