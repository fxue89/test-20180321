export interface AppSettings {
    readonly googleApiKey: string
    readonly basePath: string // for routing purposes
}

export const appSettings: AppSettings = process.env.appSettings as any; 