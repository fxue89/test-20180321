import * as path from 'path'
import * as merge from 'webpack-merge'
import { createDevConfig } from './config/webpack.dev'
import { createProdConfig } from "./config/webpack.prod"
import { createTestConfig } from "./config/webpack.test"
import { commonConfig } from "./config/webpack.common"

const root = path.resolve(__dirname, '.');
const sourcePath = path.join(root, './src');
const outPath = path.join(root, './dist');

const appSettings = {
  basePath: process.env.BASE_PATH || '/',
  googleApiKey: process.env.GOOGLE_API_KEY || 'AIzaSyDxkhXDnXbHKv8Gz-H1hVKknXC1kWJ9goI',
}

switch (process.env.NODE_ENV) {
  case 'production':
    module.exports = merge(commonConfig(appSettings, sourcePath, outPath), createProdConfig(sourcePath));
    break;
  case 'test':
    module.exports = merge(commonConfig(appSettings, sourcePath, outPath), createTestConfig(sourcePath));
    break;
  case 'development':
  default:
    module.exports = merge(commonConfig(appSettings, sourcePath, outPath), createDevConfig(sourcePath));
}